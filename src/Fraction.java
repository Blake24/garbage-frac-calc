public class Fraction {
    private static String answer;

    public static String add(int wholeNumb1, int num1, int denom1, int wholeNumb2, int num2, int denom2){
        int unredDenom = denom1 * denom2;
        int equivNum1 = num1 * denom2;
        int equivNum2 = num2 * denom1;
        int unredNum = equivNum1 + equivNum2;
        int finalWholeNumb = wholeNumb1 + wholeNumb2;

        while (unredNum > unredDenom){
            unredNum -= unredDenom;
            finalWholeNumb++;
        }

        if (unredNum == unredDenom){
            finalWholeNumb++;
            answer = "" + finalWholeNumb;
            return answer;
        }

        int set = 0;
        int GCD = unredDenom;
        while (set == 0) {

            if (unredNum % GCD == 0 && unredDenom % GCD == 0) {
                int finalNum = unredNum / GCD;
                int finalDenom = unredDenom / GCD;

                if (finalNum == 0){
                    answer = "" + finalWholeNumb;
                }
                else{
                    answer = finalWholeNumb + " " + finalNum + "/" + finalDenom;
                }
                set = 1;
            }
            else{
                GCD--;
            }
        }

        return answer;
    }

    public static String subtract(int wholeNumb1, int num1, int denom1, int wholeNumb2, int num2, int denom2){

        int unredDenom = denom1 * denom2;
        int equivNum1 = num1 * denom2;
        int equivNum2 = num2 * denom1;
        int unredNum = equivNum1 - equivNum2;
        int finalWholeNumb = wholeNumb1 - wholeNumb2;

        while (unredNum > unredDenom){
            unredNum -= unredDenom;
            finalWholeNumb++;
        }

        if (unredNum == unredDenom){
            finalWholeNumb++;
            answer = "" + finalWholeNumb;
            return answer;
        }

        int finalNum;
        int finalDenom;

        int set = 0;
        int GCD = unredDenom;
        while (set == 0) {

            if (unredNum % GCD == 0 && unredDenom % GCD == 0) {

                finalNum = unredNum / GCD;
                finalDenom = unredDenom / GCD;

                if (finalNum == 0){
                    answer = "" + finalWholeNumb;
                }
                else{
                    answer = finalWholeNumb + " " + finalNum + "/" + finalDenom;
                }

                set = 1;
            }
            else{
                GCD--;
            }
        }
        return answer;
    }

    public static String multiply(int wholeNumb1, int num1, int denom1, int wholeNumb2, int num2, int denom2){
        int unredNum = num1 * num2;
        int unredDenom = denom1 * denom2;
        int finalWholeNumb = wholeNumb1 * wholeNumb2;

        while (unredNum > unredDenom){
            unredNum -= unredDenom;
            finalWholeNumb++;
        }

        if (unredNum == unredDenom){
            finalWholeNumb++;
            answer = "" + finalWholeNumb;
            return answer;
        }

        int set = 0;
        int GCD = unredDenom;
        while (set == 0) {

            if (unredNum % GCD == 0 && unredDenom % GCD == 0) {
                int finalNum = unredNum / GCD;
                int finalDenom = unredDenom / GCD;

                if (finalNum == 0){
                    answer = "" + finalWholeNumb;
                }
                else{
                    answer = finalWholeNumb + " " + finalNum + "/" + finalDenom;
                }
                set = 1;
            }
            else{
                GCD--;
            }
        }

        return answer;
    }


    public static String divide(int wholeNumb1, int num1, int denom1, int wholeNumb2, int num2, int denom2){
        int unredNum = num1 * denom2;
        int unredDenom = denom1 * num2;
        int finalWholeNumb = wholeNumb1 / wholeNumb2;

        while (unredNum > unredDenom){
            unredNum -= unredDenom;
            finalWholeNumb++;
        }

        if (unredNum == unredDenom){
            finalWholeNumb++;
            answer = "" + finalWholeNumb;
            return answer;
        }

        int set = 0;
        int GCD = unredDenom;
        while (set == 0) {

            if (unredNum % GCD == 0 && unredDenom % GCD == 0) {
                int finalNum = unredNum / GCD;
                int finalDenom = unredDenom / GCD;

                if (finalNum == 0){
                    answer = "" + finalWholeNumb;
                }
                else{
                    answer = finalWholeNumb + " " + finalNum + "/" + finalDenom;
                }
                set = 1;
            }
            else{
                GCD--;
            }
        }

        return answer;
    }

}
